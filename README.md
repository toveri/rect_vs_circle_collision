# What is this?

A very simple test program to get an estimation of the speeds of collision checking in rust using:

- Single vs nested if expressions.
- Squaring by multiplying vs using powi(2).
- Rectangles vs circles.

Sample run output:

```#
Running 50 iterations with 5000 colliders.
Single if rect avg duration: 102.286127ms collision count: 11939300
Nested if rect avg duration: 103.886301ms collision count: 11939300
Multiply circle avg duration: 13.82393ms collision count: 11177300
Pow circle avg duration: 13.82851ms collision count: 11177300
```

## Conclusions

- Nested if expressions seem (very) slightly slower.
- Just use the power function, there is no speed penalty and it is more readable.
- Rectangles are significantly slower compared to circles.