extern crate rand;
use std::time::{Duration, Instant};

use rand::Rng;

const ITERATIONS: u32 = 50;
const COLLIDER_COUNT: usize = 5_000;
const MAX_POS: f32 = 1000.0;
const MAX_SIZE: f32 = MAX_POS / 10.0;
fn main() {
	println!(
		"Running {} iterations with {} colliders.",
		ITERATIONS, COLLIDER_COUNT
	);
	let rectangles = generate_rectangles();
	let mut rect_duration = Duration::from_secs(0);
	let mut rect_count = 0;
	let mut rect_nested_duration = Duration::from_secs(0);
	let mut rect_nested_count = 0;
	for _ in 0..ITERATIONS / 2 {
		let (duration, count) = check_rectangles(&rectangles);
		rect_duration += duration;
		rect_count += count;
		let (duration, count) = check_rectangles_nested(&rectangles);
		rect_nested_duration += duration;
		rect_nested_count += count;
	}
	for _ in 0..ITERATIONS / 2 {
		let (duration, count) = check_rectangles_nested(&rectangles);
		rect_nested_duration += duration;
		rect_nested_count += count;
		let (duration, count) = check_rectangles(&rectangles);
		rect_duration += duration;
		rect_count += count;
	}
	rect_duration /= ITERATIONS;
	println!(
		"Single if rect avg duration: {:?} collision count: {}",
		rect_duration, rect_count
	);
	rect_nested_duration /= ITERATIONS;
	println!(
		"Nested if rect avg duration: {:?} collision count: {}",
		rect_nested_duration, rect_nested_count
	);

	let mut circlem_duration = Duration::from_secs(0);
	let mut circlem_count = 0;
	let mut circlep_duration = Duration::from_secs(0);
	let mut circlep_count = 0;
	let circles = generate_circles();
	for _ in 0..ITERATIONS / 2 {
		let (duration, count) = check_circles_multiply(&circles);
		circlem_duration += duration;
		circlem_count += count;
		let (duration, count) = check_circles_power(&circles);
		circlep_duration += duration;
		circlep_count += count;
	}
	for _ in 0..ITERATIONS / 2 {
		let (duration, count) = check_circles_power(&circles);
		circlep_duration += duration;
		circlep_count += count;
		let (duration, count) = check_circles_multiply(&circles);
		circlem_duration += duration;
		circlem_count += count;
	}
	circlem_duration /= ITERATIONS;
	println!(
		"Multiply circle avg duration: {:?} collision count: {}",
		circlem_duration, circlem_count
	);
	circlep_duration /= ITERATIONS;
	println!(
		"Pow circle avg duration: {:?} collision count: {}",
		circlep_duration, circlep_count
	);
}

#[derive(Debug)]
struct Rectangle {
	x: f32,
	y: f32,
	w: f32,
	h: f32,
}

fn generate_rectangles() -> Vec<Rectangle> {
	let mut rng = rand::thread_rng();
	let mut rectangles: Vec<Rectangle> = Vec::new();
	for _ in 0..COLLIDER_COUNT {
		let rectangle = Rectangle {
			x: rng.gen_range(0.0, MAX_POS),
			y: rng.gen_range(0.0, MAX_POS),
			w: rng.gen_range(0.0, MAX_SIZE),
			h: rng.gen_range(0.0, MAX_SIZE),
		};
		rectangles.push(rectangle);
	}
	rectangles
}

fn check_rectangles(rectangles: &[Rectangle]) -> (Duration, u32) {
	let mut count = 0;
	let start = Instant::now();
	for r1 in rectangles {
		for r2 in rectangles {
			if r1.x + r1.w > r2.x && r1.x < r2.x + r2.w && r1.y + r1.h > r2.y && r1.y < r2.y + r2.h {
				count += 1;
			}
		}
	}
	let end = start.elapsed();
	(end, count)
}

#[allow(clippy::collapsible_if)]
fn check_rectangles_nested(rectangles: &[Rectangle]) -> (Duration, u32) {
	let mut count = 0;
	let start = Instant::now();
	for r1 in rectangles {
		for r2 in rectangles {
			if r1.x + r1.w > r2.x {
				if r1.x < r2.x + r2.w {
					if r1.y + r1.h > r2.y {
						if r1.y < r2.y + r2.h {
							count += 1;
						}
					}
				}
			}
		}
	}
	let end = start.elapsed();
	(end, count)
}

#[derive(Debug)]
struct Circle {
	x: f32,
	y: f32,
	r: f32,
}

fn generate_circles() -> Vec<Circle> {
	let mut rng = rand::thread_rng();
	let mut circles: Vec<Circle> = Vec::new();
	for _ in 0..COLLIDER_COUNT {
		let circle = Circle {
			x: rng.gen_range(0.0, MAX_POS),
			y: rng.gen_range(0.0, MAX_POS),
			r: rng.gen_range(0.0, MAX_SIZE / 2.0),
		};
		circles.push(circle);
	}
	circles
}

fn check_circles_multiply(circles: &[Circle]) -> (Duration, u32) {
	let mut count = 0;
	let start = Instant::now();
	for c1 in circles {
		for c2 in circles {
			if (c1.x - c2.x) * (c1.x - c2.x) + (c1.y - c2.y) * (c1.y - c2.y)
				< (c1.r + c2.r) * (c1.r + c2.r)
			{
				count += 1;
			}
		}
	}
	let end = start.elapsed();
	(end, count)
}

fn check_circles_power(circles: &[Circle]) -> (Duration, u32) {
	let mut count = 0;
	let start = Instant::now();
	for c1 in circles {
		for c2 in circles {
			if (c1.x - c2.x).powi(2) + (c1.y - c2.y).powi(2) < (c1.r + c2.r).powi(2) {
				count += 1;
			}
		}
	}
	let end = start.elapsed();
	(end, count)
}
